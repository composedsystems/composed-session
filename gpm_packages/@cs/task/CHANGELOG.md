##[0.1.1] - 2021-02-20
- Updated Future:Observe to not pass out Value or Error on timeout

##[0.1.0] - 2020-12-07
- Refactored to pass Future back with Continuation

##[0.0.1] - 2020-11-27
- First release