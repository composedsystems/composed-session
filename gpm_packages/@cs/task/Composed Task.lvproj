﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="17008000">
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Examples" Type="Folder">
			<Item Name="Example 1.lvlib" Type="Library" URL="../example/Example 1/Example 1.lvlib"/>
		</Item>
		<Item Name="GPM Packages" Type="Folder">
			<Property Name="GPM" Type="Bool">true</Property>
			<Item Name="@cs" Type="Folder">
				<Item Name="configuration" Type="Folder">
					<Item Name="Source" Type="Folder">
						<Item Name="IniFileSection" Type="Folder">
							<Item Name="IniFileSection.lvclass" Type="LVClass" URL="../gpm_packages/@cs/configuration/Source/IniFileSection/IniFileSection.lvclass"/>
						</Item>
						<Item Name="TransientConfiguration" Type="Folder">
							<Item Name="TransientConfiguration.lvclass" Type="LVClass" URL="../gpm_packages/@cs/configuration/Source/TransientConfiguration/TransientConfiguration.lvclass"/>
						</Item>
						<Item Name="Configuration.lvlib" Type="Library" URL="../gpm_packages/@cs/configuration/Source/Configuration.lvlib"/>
					</Item>
					<Item Name="CHANGELOG.md" Type="Document" URL="../gpm_packages/@cs/configuration/CHANGELOG.md"/>
					<Item Name="gpackage.json" Type="Document" URL="../gpm_packages/@cs/configuration/gpackage.json"/>
					<Item Name="LICENSE" Type="Document" URL="../gpm_packages/@cs/configuration/LICENSE"/>
					<Item Name="README.md" Type="Document" URL="../gpm_packages/@cs/configuration/README.md"/>
				</Item>
				<Item Name="event-logger" Type="Folder">
					<Item Name="Examples" Type="Folder">
						<Item Name="Logger Examples.lvlib" Type="Library" URL="../gpm_packages/@cs/event-logger/Examples/Logger Examples.lvlib"/>
					</Item>
					<Item Name="Source" Type="Folder">
						<Item Name="Buffered Log Sink" Type="Folder">
							<Item Name="Buffered Log Sink.lvclass" Type="LVClass" URL="../gpm_packages/@cs/event-logger/Source/Buffered Log Sink/Buffered Log Sink.lvclass"/>
						</Item>
						<Item Name="Composed Log" Type="Folder">
							<Item Name="Singleton Event Log" Type="Folder">
								<Item Name="Singleton Event Log.lvlib" Type="Library" URL="../gpm_packages/@cs/event-logger/Source/Composed Log/Singleton Event Log/Singleton Event Log.lvlib"/>
							</Item>
							<Item Name="Composed Log.lvlib" Type="Library" URL="../gpm_packages/@cs/event-logger/Source/Composed Log/Composed Log.lvlib"/>
						</Item>
						<Item Name="ConsoleView" Type="Folder">
							<Item Name="ConsoleView.lvlib" Type="Library" URL="../gpm_packages/@cs/event-logger/Source/ConsoleView/ConsoleView.lvlib"/>
						</Item>
						<Item Name="Filters" Type="Folder">
							<Item Name="Compound Filter" Type="Folder">
								<Item Name="Compound Filter.lvclass" Type="LVClass" URL="../gpm_packages/@cs/event-logger/Source/Filters/Compound Filter/Compound Filter.lvclass"/>
							</Item>
							<Item Name="Event Keyword Filter" Type="Folder">
								<Item Name="Event Keyword Filter.lvclass" Type="LVClass" URL="../gpm_packages/@cs/event-logger/Source/Filters/Event Keyword Filter/Event Keyword Filter.lvclass"/>
							</Item>
							<Item Name="Event Level Filter" Type="Folder">
								<Item Name="Event Level Filter.lvclass" Type="LVClass" URL="../gpm_packages/@cs/event-logger/Source/Filters/Event Level Filter/Event Level Filter.lvclass"/>
							</Item>
							<Item Name="Event Source Filter" Type="Folder">
								<Item Name="Event Source Filter.lvclass" Type="LVClass" URL="../gpm_packages/@cs/event-logger/Source/Filters/Event Source Filter/Event Source Filter.lvclass"/>
							</Item>
						</Item>
						<Item Name="IStringFormat" Type="Folder">
							<Item Name="IStringFormat.lvclass" Type="LVClass" URL="../gpm_packages/@cs/event-logger/Source/IStringFormat/IStringFormat.lvclass"/>
						</Item>
						<Item Name="LVQueue Sink" Type="Folder">
							<Item Name="LVQueue Sink.lvclass" Type="LVClass" URL="../gpm_packages/@cs/event-logger/Source/LVQueue Sink/LVQueue Sink.lvclass"/>
						</Item>
						<Item Name="String Control Sink" Type="Folder">
							<Item Name="String Control Sink.lvclass" Type="LVClass" URL="../gpm_packages/@cs/event-logger/Source/String Control Sink/String Control Sink.lvclass"/>
						</Item>
						<Item Name="String Formats" Type="Folder">
							<Item Name="String Expression Format" Type="Folder">
								<Item Name="String Expression Format.lvclass" Type="LVClass" URL="../gpm_packages/@cs/event-logger/Source/String Formats/String Expression Format/String Expression Format.lvclass"/>
							</Item>
						</Item>
						<Item Name="String Log Sink" Type="Folder">
							<Item Name="String Log Sink.lvclass" Type="LVClass" URL="../gpm_packages/@cs/event-logger/Source/String Log Sink/String Log Sink.lvclass"/>
						</Item>
						<Item Name="SystemLog Sink" Type="Folder">
							<Item Name="SystemLog Sink.lvclass" Type="LVClass" URL="../gpm_packages/@cs/event-logger/Source/SystemLog Sink/SystemLog Sink.lvclass"/>
						</Item>
						<Item Name="Text File Sink" Type="Folder">
							<Item Name="Text File" Type="Folder">
								<Item Name="Text File.lvclass" Type="LVClass" URL="../gpm_packages/@cs/event-logger/Source/Text File Sink/Text File/Text File.lvclass"/>
							</Item>
							<Item Name="Text File Sink.lvclass" Type="LVClass" URL="../gpm_packages/@cs/event-logger/Source/Text File Sink/Text File Sink.lvclass"/>
						</Item>
					</Item>
					<Item Name="CHANGELOG.md" Type="Document" URL="../gpm_packages/@cs/event-logger/CHANGELOG.md"/>
					<Item Name="gpackage.json" Type="Document" URL="../gpm_packages/@cs/event-logger/gpackage.json"/>
					<Item Name="LICENSE" Type="Document" URL="../gpm_packages/@cs/event-logger/LICENSE"/>
					<Item Name="Readme.md" Type="Document" URL="../gpm_packages/@cs/event-logger/Readme.md"/>
				</Item>
			</Item>
		</Item>
		<Item Name="Continuation.lvlib" Type="Library" URL="../source/Continuation/Continuation.lvlib"/>
		<Item Name="Future Transport.lvlib" Type="Library" URL="../source/Future Notification/Future Transport.lvlib"/>
		<Item Name="Future.lvlib" Type="Library" URL="../source/Future/Future.lvlib"/>
		<Item Name="Promise.lvlib" Type="Library" URL="../source/Promise/Promise.lvlib"/>
		<Item Name="Request.lvlib" Type="Library" URL="../source/Request/Request.lvlib"/>
		<Item Name="Task Types.lvlib" Type="Library" URL="../source/Task Types/Task Types.lvlib"/>
		<Item Name="Task.lvlib" Type="Library" URL="../source/Task/Task.lvlib"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
				<Item Name="Actor Framework.lvlib" Type="Library" URL="/&lt;vilib&gt;/ActorFramework/Actor Framework.lvlib"/>
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="Get LV Class Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Name.vi"/>
				<Item Name="Get System Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/sysdir.llb/Get System Directory.vi"/>
				<Item Name="High Resolution Relative Seconds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/High Resolution Relative Seconds.vi"/>
				<Item Name="LVDateTimeRec.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVDateTimeRec.ctl"/>
				<Item Name="LVNumericRepresentation.ctl" Type="VI" URL="/&lt;vilib&gt;/numeric/LVNumericRepresentation.ctl"/>
				<Item Name="NI_Data Type.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/Data Type/NI_Data Type.lvlib"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_LVConfig.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/config.llb/NI_LVConfig.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="NI_SystemLogging.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/SystemLogging/NI_SystemLogging.lvlib"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="Stall Data Flow.vim" Type="VI" URL="/&lt;vilib&gt;/Utility/Stall Data Flow.vim"/>
				<Item Name="System Directory Type.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/sysdir.llb/System Directory Type.ctl"/>
				<Item Name="Time-Delay Override Options.ctl" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delay Override Options.ctl"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="usereventprio.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/usereventprio.ctl"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
			</Item>
			<Item Name="AF Debug.lvlib" Type="Library" URL="/&lt;resource&gt;/AFDebug/AF Debug.lvlib"/>
			<Item Name="LV Config Read String.vi" Type="VI" URL="/&lt;resource&gt;/dialog/lvconfig.llb/LV Config Read String.vi"/>
			<Item Name="systemLogging.dll" Type="Document" URL="systemLogging.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
