﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="17008000">
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Example 1" Type="Folder">
			<Item Name="Hardware Session.lvlib" Type="Library" URL="../example/Example 1/Hardware Session/Hardware Session.lvlib"/>
			<Item Name="Hardware TestPanel.lvlib" Type="Library" URL="../example/Example 1/Hardware TestPanel/Hardware TestPanel.lvlib"/>
			<Item Name="iHardwareDriver.lvclass" Type="LVClass" URL="../example/Example 1/iHardwareDriver/iHardwareDriver.lvclass"/>
		</Item>
		<Item Name="GPM Packages" Type="Folder">
			<Property Name="GPM" Type="Bool">true</Property>
			<Item Name="@cs" Type="Folder">
				<Item Name="configuration" Type="Folder">
					<Item Name="Source" Type="Folder">
						<Item Name="IniFileSection" Type="Folder">
							<Item Name="IniFileSection.lvclass" Type="LVClass" URL="../gpm_packages/@cs/configuration/Source/IniFileSection/IniFileSection.lvclass"/>
						</Item>
						<Item Name="TransientConfiguration" Type="Folder">
							<Item Name="TransientConfiguration.lvclass" Type="LVClass" URL="../gpm_packages/@cs/configuration/Source/TransientConfiguration/TransientConfiguration.lvclass"/>
						</Item>
						<Item Name="Configuration.lvlib" Type="Library" URL="../gpm_packages/@cs/configuration/Source/Configuration.lvlib"/>
					</Item>
					<Item Name="CHANGELOG.md" Type="Document" URL="../gpm_packages/@cs/configuration/CHANGELOG.md"/>
					<Item Name="gpackage.json" Type="Document" URL="../gpm_packages/@cs/configuration/gpackage.json"/>
					<Item Name="LICENSE" Type="Document" URL="../gpm_packages/@cs/configuration/LICENSE"/>
					<Item Name="README.md" Type="Document" URL="../gpm_packages/@cs/configuration/README.md"/>
				</Item>
				<Item Name="event-logger" Type="Folder">
					<Item Name="Examples" Type="Folder">
						<Item Name="Logger Examples.lvlib" Type="Library" URL="../gpm_packages/@cs/event-logger/Examples/Logger Examples.lvlib"/>
					</Item>
					<Item Name="Source" Type="Folder">
						<Item Name="Buffered Log Sink" Type="Folder">
							<Item Name="Buffered Log Sink.lvclass" Type="LVClass" URL="../gpm_packages/@cs/event-logger/Source/Buffered Log Sink/Buffered Log Sink.lvclass"/>
						</Item>
						<Item Name="Composed Log" Type="Folder">
							<Item Name="Singleton Event Log" Type="Folder">
								<Item Name="Singleton Event Log.lvlib" Type="Library" URL="../gpm_packages/@cs/event-logger/Source/Composed Log/Singleton Event Log/Singleton Event Log.lvlib"/>
							</Item>
							<Item Name="Composed Log.lvlib" Type="Library" URL="../gpm_packages/@cs/event-logger/Source/Composed Log/Composed Log.lvlib"/>
						</Item>
						<Item Name="ConsoleView" Type="Folder">
							<Item Name="ConsoleView.lvlib" Type="Library" URL="../gpm_packages/@cs/event-logger/Source/ConsoleView/ConsoleView.lvlib"/>
						</Item>
						<Item Name="Filters" Type="Folder">
							<Item Name="Compound Filter" Type="Folder">
								<Item Name="Compound Filter.lvclass" Type="LVClass" URL="../gpm_packages/@cs/event-logger/Source/Filters/Compound Filter/Compound Filter.lvclass"/>
							</Item>
							<Item Name="Event Keyword Filter" Type="Folder">
								<Item Name="Event Keyword Filter.lvclass" Type="LVClass" URL="../gpm_packages/@cs/event-logger/Source/Filters/Event Keyword Filter/Event Keyword Filter.lvclass"/>
							</Item>
							<Item Name="Event Level Filter" Type="Folder">
								<Item Name="Event Level Filter.lvclass" Type="LVClass" URL="../gpm_packages/@cs/event-logger/Source/Filters/Event Level Filter/Event Level Filter.lvclass"/>
							</Item>
							<Item Name="Event Source Filter" Type="Folder">
								<Item Name="Event Source Filter.lvclass" Type="LVClass" URL="../gpm_packages/@cs/event-logger/Source/Filters/Event Source Filter/Event Source Filter.lvclass"/>
							</Item>
						</Item>
						<Item Name="IStringFormat" Type="Folder">
							<Item Name="IStringFormat.lvclass" Type="LVClass" URL="../gpm_packages/@cs/event-logger/Source/IStringFormat/IStringFormat.lvclass"/>
						</Item>
						<Item Name="LVQueue Sink" Type="Folder">
							<Item Name="LVQueue Sink.lvclass" Type="LVClass" URL="../gpm_packages/@cs/event-logger/Source/LVQueue Sink/LVQueue Sink.lvclass"/>
						</Item>
						<Item Name="String Control Sink" Type="Folder">
							<Item Name="String Control Sink.lvclass" Type="LVClass" URL="../gpm_packages/@cs/event-logger/Source/String Control Sink/String Control Sink.lvclass"/>
						</Item>
						<Item Name="String Formats" Type="Folder">
							<Item Name="String Expression Format" Type="Folder">
								<Item Name="String Expression Format.lvclass" Type="LVClass" URL="../gpm_packages/@cs/event-logger/Source/String Formats/String Expression Format/String Expression Format.lvclass"/>
							</Item>
						</Item>
						<Item Name="String Log Sink" Type="Folder">
							<Item Name="String Log Sink.lvclass" Type="LVClass" URL="../gpm_packages/@cs/event-logger/Source/String Log Sink/String Log Sink.lvclass"/>
						</Item>
						<Item Name="SystemLog Sink" Type="Folder">
							<Item Name="SystemLog Sink.lvclass" Type="LVClass" URL="../gpm_packages/@cs/event-logger/Source/SystemLog Sink/SystemLog Sink.lvclass"/>
						</Item>
						<Item Name="Text File Sink" Type="Folder">
							<Item Name="Text File" Type="Folder">
								<Item Name="Text File.lvclass" Type="LVClass" URL="../gpm_packages/@cs/event-logger/Source/Text File Sink/Text File/Text File.lvclass"/>
							</Item>
							<Item Name="Text File Sink.lvclass" Type="LVClass" URL="../gpm_packages/@cs/event-logger/Source/Text File Sink/Text File Sink.lvclass"/>
						</Item>
					</Item>
					<Item Name="CHANGELOG.md" Type="Document" URL="../gpm_packages/@cs/event-logger/CHANGELOG.md"/>
					<Item Name="gpackage.json" Type="Document" URL="../gpm_packages/@cs/event-logger/gpackage.json"/>
					<Item Name="LICENSE" Type="Document" URL="../gpm_packages/@cs/event-logger/LICENSE"/>
					<Item Name="Readme.md" Type="Document" URL="../gpm_packages/@cs/event-logger/Readme.md"/>
				</Item>
				<Item Name="task" Type="Folder">
					<Item Name="example" Type="Folder">
						<Item Name="Example 1" Type="Folder">
							<Item Name="Example 1.lvlib" Type="Library" URL="../gpm_packages/@cs/task/example/Example 1/Example 1.lvlib"/>
						</Item>
					</Item>
					<Item Name="source" Type="Folder">
						<Item Name="Continuation" Type="Folder">
							<Item Name="Continuation.lvlib" Type="Library" URL="../gpm_packages/@cs/task/source/Continuation/Continuation.lvlib"/>
						</Item>
						<Item Name="Future" Type="Folder">
							<Item Name="Future.lvlib" Type="Library" URL="../gpm_packages/@cs/task/source/Future/Future.lvlib"/>
						</Item>
						<Item Name="Future Notification" Type="Folder">
							<Item Name="Future Notification" Type="Folder">
								<Item Name="Write Return Data.vi" Type="VI" URL="../gpm_packages/@cs/task/source/Future Notification/Future Notification/Write Return Data.vi"/>
							</Item>
							<Item Name="Future Transport.lvlib" Type="Library" URL="../gpm_packages/@cs/task/source/Future Notification/Future Transport.lvlib"/>
						</Item>
						<Item Name="Promise" Type="Folder">
							<Item Name="Promise.lvlib" Type="Library" URL="../gpm_packages/@cs/task/source/Promise/Promise.lvlib"/>
						</Item>
						<Item Name="Request" Type="Folder">
							<Item Name="Request.lvlib" Type="Library" URL="../gpm_packages/@cs/task/source/Request/Request.lvlib"/>
						</Item>
						<Item Name="Task" Type="Folder">
							<Item Name="Task.lvlib" Type="Library" URL="../gpm_packages/@cs/task/source/Task/Task.lvlib"/>
						</Item>
						<Item Name="Task Types" Type="Folder">
							<Item Name="Task Types.lvlib" Type="Library" URL="../gpm_packages/@cs/task/source/Task Types/Task Types.lvlib"/>
						</Item>
					</Item>
					<Item Name=".gitignore" Type="Document" URL="../gpm_packages/@cs/task/.gitignore"/>
					<Item Name="CHANGELOG.md" Type="Document" URL="../gpm_packages/@cs/task/CHANGELOG.md"/>
					<Item Name="Composed Task.lvproj" Type="Document" URL="../gpm_packages/@cs/task/Composed Task.lvproj"/>
					<Item Name="gpackage.json" Type="Document" URL="../gpm_packages/@cs/task/gpackage.json"/>
					<Item Name="LICENSE" Type="Document" URL="../gpm_packages/@cs/task/LICENSE"/>
					<Item Name="README.md" Type="Document" URL="../gpm_packages/@cs/task/README.md"/>
				</Item>
			</Item>
		</Item>
		<Item Name="Session Request Msg.lvlib" Type="Library" URL="../source/Session Request Msg/Session Request Msg.lvlib"/>
		<Item Name="Session Request.lvlib" Type="Library" URL="../source/Session Request/Session Request.lvlib"/>
		<Item Name="Session.lvlib" Type="Library" URL="../source/Session/Session.lvlib"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
				<Item Name="Actor Framework.lvlib" Type="Library" URL="/&lt;vilib&gt;/ActorFramework/Actor Framework.lvlib"/>
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Get LV Class Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Name.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get System Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/sysdir.llb/Get System Directory.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="High Resolution Relative Seconds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/High Resolution Relative Seconds.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVDateTimeRec.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVDateTimeRec.ctl"/>
				<Item Name="LVNumericRepresentation.ctl" Type="VI" URL="/&lt;vilib&gt;/numeric/LVNumericRepresentation.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="NI_Data Type.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/Data Type/NI_Data Type.lvlib"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_LVConfig.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/config.llb/NI_LVConfig.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="NI_SystemLogging.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/SystemLogging/NI_SystemLogging.lvlib"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="Stall Data Flow.vim" Type="VI" URL="/&lt;vilib&gt;/Utility/Stall Data Flow.vim"/>
				<Item Name="System Directory Type.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/sysdir.llb/System Directory Type.ctl"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Time-Delay Override Options.ctl" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delay Override Options.ctl"/>
				<Item Name="Time-Delayed Send Message Core.vi" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delayed Send Message Core.vi"/>
				<Item Name="Time-Delayed Send Message.vi" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delayed Send Message.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="usereventprio.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/usereventprio.ctl"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
			</Item>
			<Item Name="AF Debug.lvlib" Type="Library" URL="/&lt;resource&gt;/AFDebug/AF Debug.lvlib"/>
			<Item Name="LV Config Read String.vi" Type="VI" URL="/&lt;resource&gt;/dialog/lvconfig.llb/LV Config Read String.vi"/>
			<Item Name="systemLogging.dll" Type="Document" URL="systemLogging.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
