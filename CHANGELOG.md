##[1.5.0] - 2021-04-06
- Fixed reentrancy settings on VIs.
- Fixed issue with timeout in future

##[1.4.4] - 2021-02-21
- Refactored request evaluation inside Session Scheduler to better handle priority.

##[1.4.3] - 2021-02-18
- Refactored error handling on Session Scheduler and Session actors Handle Error overrides.

##[1.4.2] - 2021-02-18
- Refactored Sessions by combining Start and Init logic, both now are called from the Start message.  I removed the Init message from the project.  Combining start and init cleaned up the code from a readability point of view, and it also made error handling on Session start easier.

##[1.4.1] - 2021-02-18
- Cleaned up error handling on Session Start/Init

##[1.3.1] - 2021-01-13
- Updated Handle Error of session to clear error after logging.
- Fixed a bug with error propagation from a session service request

##[1.3.0] - 2020-12-07
- Refactored how Session Request Msgs get updated, this forces all Session Request Messages to inherit from a new Session Requst Msg ancestor.

##[1.2.1] - 2020-12-01
- Added the ability to update the Continuation in the Schedule Request Msg, useful for Prepared Schedule Request Msgs.

##[1.2.0] - 2020-11-29
- Added the ability to update a Schedule Request Msg, useful for Prepared Schedule Request Msgs.

##[1.1.1] - 2020-11-29
- Fixed a bug where the Session Scheduler was not calling Initialize Session.
- Fixed a bug where the Session Reference was not closing its local event log.

##[1.1.0] - 2020-11-28
- Added ability to Prepare a Request and then Execute it at some point in the future, useful for sequencing.

##[1.0.1] - 2020-11-27
- GPM didn't appreciate my versioning shenanigans...

##[0.0.1] - 2020-11-27
- Initial Release